import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:studentoffline/pages/signup_page.dart';


class LandingPage extends StatelessWidget{
  @override
  build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.purple,
      body: Center(
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.yellowAccent
          ),
          child: Text('SIGN UP', 
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold
          ),),
          onPressed: (){
            Navigator.of(context).push(
              CupertinoPageRoute(builder: (_)=> SignUpPage()));
          },
        ),
      ),
    );
  }
}