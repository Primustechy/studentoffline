import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:studentoffline/sqflite/db_helper.dart';
import 'package:studentoffline/students/student.dart';
import 'package:studentoffline/students/add_student.dart';

class HomePage extends StatefulWidget{
  @override
  _HomePageState createState()=> _HomePageState();
}

class _HomePageState extends State<HomePage>{

  StudentDatabaseHelper studentDatabaseHelper;
  List<Student> studentList = [];

  @override
  void initState() {
    super.initState();
    studentDatabaseHelper = StudentDatabaseHelper();
    loadStudentList();
  }

  @override
  build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Students List'),
        elevation: 0,
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton.extended(
        label: Text('ADD'),
        icon: Icon(
          Icons.person_add
        ),
        onPressed: () {
          Navigator.of(context).push(CupertinoPageRoute(builder: (_)=> AddStudentPage(null))).then((value){
            if (value) {
              loadStudentList();
            }
          });
        },
      ),
      body: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return Card(
            child: ListTile(
              contentPadding: EdgeInsets.all(8),
              title: Text(studentList[index].getName()),
              subtitle: Text(studentList[index].getDepartment()),
              trailing: IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: (){
                    deleteAStudentEntry(studentList[index].getId());
                  },
                ),

              onTap: (){
                  Navigator.of(context).push(CupertinoPageRoute(builder: (_)=> AddStudentPage(studentList[index]))).then((val){
                    if(val){
                      loadStudentList();
                    }
                  });
                },
            ),
          );
        },
        itemCount: studentList.length,
      )
    );
  }

  // FUNCTION TO HELP LOAD THE STUDENT LIST
  loadStudentList() {
    studentDatabaseHelper.getStudentsList().then((List<Student> listOfStudents){
      studentList = listOfStudents;
      setState(() {

      });
    });
  }

  // FUNCTION TO HELP DELETE FROM THE STUDENT LIST
  deleteAStudentEntry(int id) {
    studentDatabaseHelper.deleteStudent(id).then((int value){
      loadStudentList();
    });
  }
}