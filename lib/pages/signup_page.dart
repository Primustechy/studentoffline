import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:studentoffline/pages/home_page.dart';


class SignUpPage extends StatelessWidget {
  @override
  build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          children: [
            SizedBox(height: 100,),
            Text("WELCOME",
              style: TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.w900
              ),
            ),
            SizedBox(height: 40),
            Text("TO",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w900
              ),
            ),
            SizedBox(height: 40),
            Text("STUDENT PROFILE",
              style: TextStyle(
                  fontSize: 34,
                  fontWeight: FontWeight.w900
              ),
            ),
            SizedBox(height: 50),
            MaterialButton(
              color: Colors.deepOrange,
              child: Text('ENTER HOME',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: 16
                ),
              ),
              onPressed: (){
                Navigator.of(context).push(
                    CupertinoPageRoute(builder: (_)=> HomePage()));
              },
            )
          ],
        ),
      ),
    );
  }
}