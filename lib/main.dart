import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:studentoffline/pages/signup_page.dart';
import 'package:studentoffline/pages/landing_page.dart';

void main() async {

  WidgetsFlutterBinding.ensureInitialized();
  
  bool isYourFirstTimeToLaunchApp;
  SharedPreferences.getInstance().then((SharedPreferences preference){
    isYourFirstTimeToLaunchApp = preference.getBool('is_your_first_time_to_launch_app') ?? true;
    print(isYourFirstTimeToLaunchApp);
    preference.setBool('is_your_first_time_to_launch_app', false);

    runApp(MyApp(isYourFirstTimeToLaunchApp));
  });
}

class MyApp extends StatelessWidget{

  bool isYourFirstTimeToLaunchApp;

  MyApp(this.isYourFirstTimeToLaunchApp);

  @override
  build(BuildContext context) {
    return MaterialApp(
      title: "Student Offline",
      theme: ThemeData(primarySwatch: Colors.purple),
      debugShowCheckedModeBanner: false,
      home: isYourFirstTimeToLaunchApp ? LandingPage() : SignUpPage(),
    );
  }
}