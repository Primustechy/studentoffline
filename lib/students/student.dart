class Student {

  int id;
  String name;
  String email;
  String university;
  String department;
  String location;

  Student(
    this.name,
    this.email,
    this.university,
    this.department,
    this.location
  );

  Student.withId(
    this.id,
    this.name,
    this.email,
    this.university,
    this.department,
    this.location
  );

  int getId() {
    return this.id;
  }

  String getName() {
    return this.name;
  }

  String getEmail() {
    return this.email;
  }

  String getUniversity() {
    return this.university;
  }

  String getDepartment() {
    return this.department;
  }

  String getLocation() {
    return this.location;
  }

  setId(int newId) {
    this.id = newId;
  }

  setName(String newName) {
    this.name = newName;
  }

  setEmail(String newEmail) {
    this.email = newEmail;
  }

  setUniversity(String newUniversity) {
    this.university = newUniversity;
  }

  setDepartment(String newDepartment) {
    this.department = newDepartment;
  }
  
  setLocation(String newLocation) {
    this.location = newLocation;
  }

  Map<String, dynamic> toMap(){
    Map<String, dynamic> studentMap = Map<String, dynamic>();
    if (id != null) {
      studentMap['id'] = id;
    }
    studentMap['name'] = name;
    studentMap['email'] = email;
    studentMap['university'] = university;
    studentMap['department'] = department;
    studentMap['location'] = location;

    return studentMap;
  }

  Student.fromMapObject(Map<String, dynamic> studentMap){
    this.id = studentMap['id'];
    this.name = studentMap['name'];
    this.email = studentMap['email'];
    this.university = studentMap['university'];
    this.department = studentMap['department'];
    this.location = studentMap['location'];
  }

}