import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:toast/toast.dart';
import 'package:studentoffline/students/student.dart';
import 'package:studentoffline/sqflite/db_helper.dart';

class AddStudentPage extends StatefulWidget{

  Student myStudent;

  AddStudentPage(this.myStudent);

  _AddStudentPageState createState()=> _AddStudentPageState(myStudent);
}

class _AddStudentPageState extends State<AddStudentPage> {

  Student myStudent;

  _AddStudentPageState(this.myStudent);

  StudentDatabaseHelper studentDatabaseHelper = StudentDatabaseHelper();

  @override
  void initState() {
    super.initState();

    if (myStudent == null) {
      // Editing a new student
      shouldEditStudent = false;
    } else {
      // Editing an old student
      shouldEditStudent = true;
      initializeStudentTextFields();
    }
  }

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
    emailController.dispose();
    universityController.dispose();
    departmentController.dispose();
    locationController.dispose();
  }

  bool shouldEditStudent = false;
  String name;
  String email;
  String university;
  String department;
  String location;

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController universityController = TextEditingController();
  TextEditingController departmentController = TextEditingController();
  TextEditingController locationController = TextEditingController();

  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(shouldEditStudent ? 'Edit Student' : 'Add Student'),
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: (){
            Navigator.of(context).pop(true);
          },
        ),
        actions: <Widget>[
          TextButton(
            child: Text(shouldEditStudent ? 'Update' : 'Save',
                style: TextStyle(fontSize: 16, color: Colors.white)),
            onPressed: (){
              checkTextFields(context);
            },
          )
        ]
      ),

      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 20, vertical: 20
        ),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[

            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Name'
              ),
              keyboardType: TextInputType.text,
              controller: nameController,
              onChanged: (String val){
                name = val;
              },
            ),

            SizedBox(height: 8,),

            TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Email'
              ),
              keyboardType: TextInputType.emailAddress,
              controller: emailController,
              onChanged: (String val){
                email = val;
              },
            ),

            SizedBox(height: 8,),

            TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'University'
              ),
              keyboardType: TextInputType.text,
              controller: universityController,
              onChanged: (String val){
                university = val;
              },
            ),

            SizedBox(height: 8,),

            TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Department'
              ),
              keyboardType: TextInputType.text,
              controller: departmentController,
              onChanged: (String val){
                department = val;
              },
            ),

            SizedBox(height: 8,),

            TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Location'
              ),
              keyboardType: TextInputType.text,
              controller: locationController,
              onChanged: (String val){
                location = val;
              },
            ),
          ],
        )
      )
    );
  }

  initializeStudentTextFields() {
    name = nameController.text = myStudent.getName();
    email = emailController.text = myStudent.getEmail();
    university = universityController.text = myStudent.getUniversity();
    department = departmentController.text = myStudent.getDepartment();
    location = locationController.text = myStudent.getLocation();
  }

  saveStudentInfo() {
    Student student;
    if (shouldEditStudent) {
      student = Student.withId(myStudent.getId(), name, email, university, department, location);
      studentDatabaseHelper.updateNewStudent(student).then((int id){
        print(id);
        Navigator.of(context).pop(true);
      });
    }else {
      student = Student(name, email, university, department, location);
      studentDatabaseHelper.insertNewStudent(student).then((int id){
        print(id);
        Navigator.of(context).pop(true);
      });
    }
  }

  checkTextFields(BuildContext context){
    if(name == null || name == ''){
      Toast.show('Please enter a student name', context, duration: Toast.LENGTH_LONG);
      return;
    }

    if(email == null || email == ''){
      Toast.show('Please enter a valid email', context, duration: Toast.LENGTH_LONG);
      return;
    }

    if(university == null || university == ''){
      Toast.show('Please enter a university', context, duration: Toast.LENGTH_LONG);
      return;
    }

    if(department == null || department == ''){
      Toast.show('Please enter a department', context, duration: Toast.LENGTH_LONG);
      return;
    }

    if(location == null || location == ''){
      Toast.show('Please enter student location', context, duration: Toast.LENGTH_LONG);
      return;
    }else{
      saveStudentInfo();
    }
  }

}
