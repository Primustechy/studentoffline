import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:studentoffline/students/student.dart';

class StudentDatabaseHelper {
  static StudentDatabaseHelper _studentDatabaseHelper;
  static Database _database;

  // here we create the students table
  String studentTable = 'student_table';

  // here we create a field for the students table
  String colId = 'id';
  String colName = 'name';
  String colEmail = 'email';
  String colUniversity = 'university';
  String colDepartment = 'department';
  String colLocation = 'location';

  // CREATE THE INSTANCE OF THE STUDENT DATABASE
  StudentDatabaseHelper._createInstance();

  factory StudentDatabaseHelper(){
    if (_studentDatabaseHelper == null) {
      _studentDatabaseHelper = StudentDatabaseHelper._createInstance();
    }
    return _studentDatabaseHelper;
  }
  
  // FUNCTION TO GET THE DATABASE
  Future<Database> get database async{
    if(_database == null){
      _database = await initializeDatabase();
    }
    return _database;
  }

  // FUNCTION TO INITIALIZE THE DATABASE
  Future<Database> initializeDatabase() async{
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'students.db';

    Database studentsDatabase = await openDatabase(path, version: 1, onCreate: _createDatabase);
    return studentsDatabase;
  }

  // FUNCTION TO CREATE THE DATABASE
  void _createDatabase(Database db, int newVersion) async {
    await db.execute('CREATE TABLE $studentTable($colId INTEGER PRIMARY KEY AUTOINCREMENT, $colName TEXT, $colEmail TEXT, $colUniversity TEXT, $colDepartment TEXT, $colLocation TEXT,)');
  }

  // FUNCTION TO ADD A NEW STUDENT TO THE DATABASE
  Future<int> insertNewStudent(Student student) async{
    Database db = await this.database;
    int result = await db.insert(studentTable, student.toMap());
    return result;
  }

  // FUNCTION TO UPDATE THE DATABASE
  Future<int> updateNewStudent(Student student) async{
    Database db = await this.database;
    int result = await db.update(studentTable, student.toMap(), where: '$colId = ?', whereArgs: [student.id]);
    return result;
  }

  // FUNCTION TO DELETE THE DATABASE
  Future<int> deleteStudent(int id) async {
    Database db = await this.database;
    //int result = await db.rawDelete('DELETE FROM $studentTable WHERE $colId = $id');
    int result = await db.delete(studentTable, where: '$colId = ?', whereArgs: ['id']);
    return result;
  }

  // FUNCTION TO GET THE COUNT OF THE STUDENT IN THE DATABASE
  Future<int> getCount() async {
    Database db = await this.database;
    List<Map<String, dynamic>> x = await db.rawQuery('SELECT COUNT (*) from $studentTable');
    int result = Sqflite.firstIntValue(x);
    return result;
  }

  // FUNCTION TO GET A LIST OF ALL STUDENTS IN THE DATABASE
  Future<List<Student>> getStudentsList() async {
    List<Map<String, dynamic>> studentMapList = await getStudentMapList();
    int count = studentMapList.length;

    List<Student> studentList = [];

    for(int i = 0; i < count; i++){
      studentList.add(Student.fromMapObject(studentMapList[i]));
    }

    return studentList;
  }
  
  // FUNCTION TO GET THE LIST OF STUDENT AS A MAP
  Future<List<Map<String, dynamic>>> getStudentMapList() async {
    Database db = await this.database;

    List<Map<String, dynamic>> result = await db.query(studentTable);
    return result;
  }

}
